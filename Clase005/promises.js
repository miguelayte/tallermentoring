const Personal =[
    {id:"1",nombre:"Sandra",edad:"23"},
    {id:"2",nombre:"Sara",edad:"25"},
    {id:"3",nombre:"Enid",edad:"21"},
]

const Cargo = [
    {id:"1",cargo:"Marketing"},
    {id:"2",cargo:"Diseño"},
    {id:"3",cargo:"Sistemas"}
]

obtenerPersonal=(id)=>{
    return new Promise((resolve, reject)=>{
        const listPersonal=Personal.find(personal => personal.id==id);
        if (!listPersonal) {
            return reject('NO SE ENCONTRARON DATOS');
        }
        resolve(listPersonal);
    })
}

obtenerCargo=(id)=>{
    return new Promise((resolve, reject)=>{
        const listCargo=Cargo.find(cargo => cargo.id==id);
        if (!listCargo) {
            return reject('NO SE ENCONTRARON DATOS');
        }
        resolve(listCargo);
    })
}

let Id=2;

// obtenerPersonal(Id)
// .then(result=>{
//     console.log("Personal: ",result);
//     obtenerCargo(result.id)
//     .then(result=>{
//         console.log("Cargo: ",result);
//     })
// })
// .catch(err=>console.log(err));

// obtenerPersonal(Id)
// .then(personal=>{
//     obtenerCargo(personal.id)
//     .then(cargo=>{
//         console.log(`Personal ${personal.nombre} y su cargo es ${cargo.cargo}`);
//     })
// })
// .catch(err=>console.log(err));

// obtenerPersonal(Id)
// .then(personal=>{
//     obtenerCargo(personal.id)
//     .then(cargo=>{
//         let valor ={
//             personal: personal.nombre,
//             cargo: cargo.cargo
//         }
//         console.log("valor: ",valor);
//         console.log(`Personal ${personal.nombre} y su cargo es ${cargo.cargo}`);
//     })
// })
// .catch(err=>console.log(err));

//---------- Obtener personal con el ASSIGN y AWAIT






