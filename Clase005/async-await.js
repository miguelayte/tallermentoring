//---- Creando los objetos
const Personal =[
    {id:"1",nombre:"Sandra",edad:"23"},
    {id:"2",nombre:"Sara",edad:"25"},
    {id:"3",nombre:"Enid",edad:"21"},
]

const Cargo = [
    {id:"1",cargo:"Marketing"},
    {id:"2",cargo:"Diseño"},
    {id:"3",cargo:"Sistemas"}
]

//----- Creando la Promesa
async function obtenerPersonal(id) {
    try {
        const listaPersonal = await Personal.find(persona => persona.id==id);
        return listaPersonal;
    } catch (e) {
        return e;
    }
}

async function obtenerPersonal(id) {
    try {
        const listaPersonal = await Personal.find(persona => persona.id==id);
        let datos = listaPersonal.nombre;
        return listaPersonal;
    } catch (e) {
        return e;
    }
}


//---- Consumiendo la promesa
let id=2;

//---- Consumiendo promesa con async y await, requiere una función (Usado generalmente en el Backend)
async function main() {
    const datos = await obtenerPersonal(id);
    console.log(datos);
}
main()

//---- Consumiendo promesa con then (Usado generalmente en el Front)
// obtenerPersonal(id)
// .then(personal=>{
//     console.log(personal);
// })






