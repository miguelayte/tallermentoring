/*=======================================================
SECCION REQIERE
==========================================================*/ 
const express=require('express');
const mongoose = require('mongoose');

const app=express();

/*=======================================================
SECCION MIDDLEWARE
==========================================================*/ 
app.use(express.json());

/*=======================================================
SECCION RUTAS
==========================================================*/ 
//app.use('/api/personal',require('./routers/personal'));
//app.use('/api/index',require('./routers/index'));



/*=======================================================
SECCION DATABASE
==========================================================*/ 
mongoose.connect('mongodb://localhost:27017/db_admin', {useNewUrlParser: true},(err,res)=>{
    if (err)  throw err;
    console.log("conectado corectamente");
});


/*=======================================================
SECCION PUERTO
==========================================================*/ 
app.set('port',process.env.PORT||7000);

/*=======================================================
SECCION SERVIDPR
==========================================================*/ 
app.listen(app.get('port'),()=>{
    console.log(`Escuchando el puerto ${app.get('port')}`);
})


