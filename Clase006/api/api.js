//---------- Si se pasa POST
// let user={
//     user: 'miguelayte'
// }

// fetch(`https://gitlab.com/api/v4/users`,{
//     method: 'POST',
//     headers:{'Content-Type':'application/joson'},
//     body:user
// })

//---------- Si se pasa GET
// const fetch=require('node-fetch')
// let user='miguelayte';

// function userGit(username) {
//     fetch(`https://gitlab.com/api/v4/users?username=${username}`)
//     .then(res=>res.json())
//     .then(usuario=>{
//         console.log(usuario);
//     })       
//     .catch(err=>console.log(err));
// }

// userGit(user);

//-------- Si se pasa GET y consumiendo con async await
// const fetch=require('node-fetch')
// let user='miguelayte';

// async function userGit(username) {
//     try {
//         const user = await fetch(`https://gitlab.com/api/v4/users?username=${username}`)
//         const userdatos = await user.json()
//         //console.log(userdatos);
//         return userdatos;
//     } catch (e) {
//         return console.log(e);
//     }
// }

// userGit(user);

//-------- Si se pasa GET y consumiendo con async await
const fetch=require('node-fetch')
let user='miguelayte';

async function userGit(username) {
    try {
        const user = await fetch(`https://gitlab.com/api/v4/users?username=${username}`)
        const userdatos = await user.json()
        //console.log(userdatos);
        return userdatos;
    } catch (e) {
        return console.log(e);
    }
}

//--- Leyendo con async await
async function main() {
    try {
        const datos=await userGit(user);
        console.log(datos);
    } catch (e) {
        console.log(e);
    }
}

main();

//--- Leyendo con promesas
// userGit(user)
// .then(respuesta=>{
//     console.log(respuesta);
// })
// .catch(err=>console.log(respuesta);)


