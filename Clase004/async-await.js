async function manejadordepeticiones(req,res) {
    try {
        const usuario = await Usuario.findOne({_id:req.body.id});
        if (!usuario) {
            return res.status(404).json({ok:false,mensaje:'ID NO ENCONTRADO'})
        }
        const cursito = await Cursos.find(usuario._id);
        res.status(200).json(cursito);
    } catch (e) {
        //res.status(500).json({ok:false,mensaje:'ERROR EN PROCESO'})
        res.status(500).json({ok:false,mensaje:e})
    }
}